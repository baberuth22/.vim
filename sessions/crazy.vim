" ~/.vim/sessions/crazy.vim:
" Vim session script.
" Created by session.vim 2.4.8 on 15 October 2013 at 13:44:55.
" Open this file in Vim and run :source % to restore your session.

set guioptions=egmrL
silent! set guifont=
if exists('g:syntax_on') != 1 | syntax on | endif
if exists('g:did_load_filetypes') != 1 | filetype on | endif
if exists('g:did_load_ftplugin') != 0 | filetype plugin off | endif
if exists('g:did_indent_on') != 0 | filetype indent off | endif
if &background != 'dark'
	set background=dark
endif
if !exists('g:colors_name') || g:colors_name != 'desert' | colorscheme desert | endif
call setqflist([])
let SessionLoad = 1
if &cp | set nocp | endif
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
cd ~/
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +14 /Applications/MacVim.app/Contents/Resources/vim/vimrc
badd +9 .vim/UltiSnips/php.snippets
badd +25 /Volumes/vhosts/axia-db/app/View/MerchantRejects/index.ctp
badd +225 /Volumes/vhosts/axia-db/app/Controller/MerchantRejectsController.php
badd +258 /Volumes/vhosts/axia-db/app/Model/MerchantReject.php
badd +38 /Volumes/vhosts/axia-db/app/Controller/AppController.php
badd +3 .vim/UltiSnips/ctp.snippets
badd +25 .vim/UltiSnips/html.snippets
badd +17 /Volumes/vhosts/axia-db/app/Plugin/Filter/View/Elements/filter_form_fields.ctp
badd +218 /Volumes/vhosts/axia-db/app/Plugin/Filter/Controller/Component/FilterComponent.php
badd +42 /Volumes/vhosts/axia-db/app/Plugin/Filter/Model/Behavior/FilteredBehavior.php
badd +1 /Volumes/vhosts/axia-db/app/Plugin/Filter/Model/FilterAppModel.php
badd +10 /Volumes/vhosts/axia-db/app/View/MerchantRejects/edit.ctp
badd +1 /Volumes/vhosts/axia-db/app/Plugin/Filter/Controller/FilterAppController.php
badd +45 /Volumes/vhosts/axia-db/app/Model/AppModel.php
badd +10 /Volumes/vhosts/axia-db/app/Controller/Component/JediComponent.php
badd +1 /Volumes/vhosts/axia-db/app/Controller/Component/AccessControlComponent.php
badd +1 /Volumes/vhosts/axia-db/app/Controller/Component/JediComponent
badd +1 /Volumes/vhosts/axia-db/app/Controller/Component/FlagStatusLogicComponent.php
badd +8 .NERDTreeBookmarks
badd +23 /Volumes/vhosts/axia-db/app/View/MerchantRejects/edit-save.php
badd +13 /Volumes/vhosts/axia-db/app/webroot/js/merchant-reject-edit.js
badd +193 /Volumes/vhosts/axia-db/app/Config/bootstrap.php
silent! argdel *
edit /Volumes/vhosts/axia-db/app/Plugin/Filter/Controller/Component/FilterComponent.php
set splitbelow splitright
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
2wincmd h
wincmd w
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 115 + 134) / 269)
exe 'vert 3resize ' . ((&columns * 115 + 134) / 269)
" argglobal
enew
" file NERD_tree_1
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 218 - ((0 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
218
normal! 056|
wincmd w
" argglobal
edit /Volumes/vhosts/axia-db/app/Plugin/Filter/Model/Behavior/FilteredBehavior.php
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 91 - ((0 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
91
normal! 0
wincmd w
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 115 + 134) / 269)
exe 'vert 3resize ' . ((&columns * 115 + 134) / 269)
tabedit /Volumes/vhosts/axia-db/app/Model/MerchantReject.php
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 231 + 134) / 269)
" argglobal
enew
" file NERD_tree_1
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 24 - ((23 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
24
normal! 0
wincmd w
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 231 + 134) / 269)
tabedit /Volumes/vhosts/axia-db/app/View/MerchantRejects/edit.ctp
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 231 + 134) / 269)
" argglobal
enew
" file NERD_tree_1
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 0
wincmd w
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 231 + 134) / 269)
tabedit /Volumes/vhosts/axia-db/app/Controller/AppController.php
set splitbelow splitright
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
2wincmd h
wincmd w
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 115 + 134) / 269)
exe 'vert 3resize ' . ((&columns * 115 + 134) / 269)
" argglobal
enew
" file NERD_tree_1
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 12 - ((8 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
12
normal! 024|
wincmd w
" argglobal
edit /Volumes/vhosts/axia-db/app/Config/bootstrap.php
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 9 - ((8 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
9
normal! 013|
wincmd w
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 115 + 134) / 269)
exe 'vert 3resize ' . ((&columns * 115 + 134) / 269)
tabedit /Volumes/vhosts/axia-db/app/webroot/js/merchant-reject-edit.js
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 231 + 134) / 269)
" argglobal
enew
" file NERD_tree_1
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 9 - ((8 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
9
normal! 040|
wincmd w
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 231 + 134) / 269)
tabedit /Volumes/vhosts/axia-db/app/Controller/Component/JediComponent.php
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 231 + 134) / 269)
" argglobal
enew
" file NERD_tree_1
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 0
wincmd w
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 231 + 134) / 269)
tabedit .vim/UltiSnips/php.snippets
set splitbelow splitright
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
3wincmd h
wincmd w
wincmd w
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 76 + 134) / 269)
exe 'vert 3resize ' . ((&columns * 76 + 134) / 269)
exe 'vert 4resize ' . ((&columns * 77 + 134) / 269)
" argglobal
enew
" file NERD_tree_1
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 20 - ((17 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
20
normal! 032|
wincmd w
" argglobal
edit .vim/UltiSnips/html.snippets
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 20 - ((0 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
20
normal! 030|
wincmd w
" argglobal
edit .vim/UltiSnips/php.snippets
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 0
wincmd w
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 76 + 134) / 269)
exe 'vert 3resize ' . ((&columns * 76 + 134) / 269)
exe 'vert 4resize ' . ((&columns * 77 + 134) / 269)
tabedit /Applications/MacVim.app/Contents/Resources/vim/vimrc
set splitbelow splitright
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
2wincmd h
wincmd w
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 115 + 134) / 269)
exe 'vert 3resize ' . ((&columns * 115 + 134) / 269)
" argglobal
enew
" file NERD_tree_1
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 153 - ((16 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
153
normal! 052|
wincmd w
" argglobal
edit /Applications/MacVim.app/Contents/Resources/vim/vimrc
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 02|
wincmd w
3wincmd w
exe 'vert 1resize ' . ((&columns * 37 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 115 + 134) / 269)
exe 'vert 3resize ' . ((&columns * 115 + 134) / 269)
tabnext 8
if exists('s:wipebuf')
"   silent exe 'bwipe ' . s:wipebuf
endif
" unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToO
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save

" Support for special windows like quick-fix and plug-in windows.
" Everything down here is generated by vim-session (not supported
" by :mksession out of the box).

tabnext 1
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTree ~/
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 37|2resize 75|vert 2resize 115|3resize 75|vert 3resize 115|
tabnext 2
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 37|2resize 75|vert 2resize 231|
tabnext 3
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 37|2resize 75|vert 2resize 231|
tabnext 4
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 37|2resize 75|vert 2resize 115|3resize 75|vert 3resize 115|
tabnext 5
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 37|2resize 75|vert 2resize 231|
tabnext 6
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 37|2resize 75|vert 2resize 231|
tabnext 7
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 37|2resize 75|vert 2resize 76|3resize 75|vert 3resize 76|4resize 75|vert 4resize 77|
tabnext 8
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 37|2resize 75|vert 2resize 115|3resize 75|vert 3resize 115|
tabnext 8
3wincmd w
if exists('s:wipebuf')
  if empty(bufname(s:wipebuf))
if !getbufvar(s:wipebuf, '&modified')
  let s:wipebuflines = getbufline(s:wipebuf, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:wipebuf
  endif
endif
  endif
endif
doautoall SessionLoadPost
unlet SessionLoad
" vim: ft=vim ro nowrap smc=128
